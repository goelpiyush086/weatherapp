//
//  SearchDataModel.swift
//  WeatherApp
//
//  Created by PG on 6/12/23.
//

import Foundation

//MARK:- SearchDataModel
struct SearchDataModel : Codable, Hashable {
    let name : String?
    let lat : Double?
    let lon : Double?
    let country : String?
    let state : String?
}
