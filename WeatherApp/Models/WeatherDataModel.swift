//
//  WeatherDataModel.swift
//  WeatherApp
//
//  Created by PG on 6/12/23.
//

import Foundation

//MARK:- WeatherDataModel
struct WeatherDataModel : Codable {
    let coord : Coordinates?
    let weather : [WeatherData]?
    let base : String?
    let visibility : Int?
    let dt : Int?
    let timezone: Int?
    let id : Int?
    let name : String?
    let cod : Int?
    let main: MainData?
    let rain : RainData?
    let wind : WindData?
    let clouds : CloudsData?
    let sys : SysData?
}
//MARK:- Coordinates
struct Coordinates : Codable{
    let lon : Double?
    let lat : Double?
}
//MARK:- WeatherData
struct WeatherData : Codable{
    let id: Int?
    let main : String?
    let description: String?
    let icon: String?
}
//MARK:- MainData
struct MainData : Codable{
    
    let temp: Double?
    let feels_like: Double?
    let temp_min: Double?
    let temp_max: Double?
    let pressure: Int?
    let humidity: Int?
    let sea_level: Double?
    let grnd_level: Double?
    
}
//MARK:- WindData
struct WindData : Codable{
    let speed : Double?
    let deg : Int?
    let gust : Double?
}
//MARK:- RainData
struct RainData : Codable{
    let rainHour : Int?
    init(rainHour: Int?) {
        self.rainHour = rainHour
    }
    
    enum codingKeys : String , CodingKey{
        case rainHour = "1h"
    }
}
//MARK:- CloudsData
struct CloudsData : Codable{
    let all : Int?
}
//MARK:- SysData
struct SysData : Codable{
    let type ,id , sunrise ,sunset : Int?
    let country: String?
}
